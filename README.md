## Transactions and Accounts APIs

This project implements endpoints to manage monetary transactions as a simulation of a credit card operator.

### Architecture

It consists of two code bases (accounts service and transactions service) that has two APIs (one to each domain) and a subscriber of `created-accounts` events emitted by accounts service to create corresponding wallets on transaction service. Each one of these workloads can be scaled out horizontally because it has been delivered in containers.

The accounts API, has a endpoint to create accounts that persists its data in a PostgreSQL database and publishes to a RabbitMQ message broker. There is also, an endpoint to get accounts by id.

The transactions API has an endpoint to create transactions that validates the transaction by checking if there is a wallet for that account id and checking the operation type and transaction amount. All these data are persisted in another PostgreSQL database.

There is a transaction subcriber to listen accounts created events in order to create corresponding wallets on PostgreSQL database.

### How to run

Create the `.env` file and define the desired HTTP port by setting `API_GATEWAY_PORT` variable.

```bash
cp .env.example .env
```

Run `docker-compose up` (at first time it may take some time to download all images)

### How to test

Import the file `Pismo.postman_collection.json` to Postman, check the `{{apiUrl}}` variable to meet the same port as defined in `API_GATEWAY_PORT` and start to play any of those requests.

### Development

Each code base has it own makefile to execute some development tasks like test or lint.

-   [Accounts Service](accounts-service/README.md)
-   [Transactions Service](transactions-service/README.md)
