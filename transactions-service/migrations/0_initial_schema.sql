CREATE TABLE IF NOT EXISTS wallets (
    id SERIAL PRIMARY KEY,
    external_id CHAR(36) NOT NULL UNIQUE,
    account_id CHAR(36) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS operation_types (
    id SERIAL PRIMARY KEY,
    external_id VARCHAR(36) NOT NULL UNIQUE,
    description VARCHAR(100) NOT NULL,
    is_negative BOOLEAN DEFAULT TRUE
);

INSERT INTO operation_types (external_id, description, is_negative)
VALUES
    ('cash-purchase', 'COMPRA À VISTA', TRUE),
    ('installment-purchase', 'COMPRA PARCELADA', TRUE),
    ('cash-withdrawal', 'SAQUE', TRUE),
    ('payment', 'PAGAMENTO', FALSE);

CREATE TABLE IF NOT EXISTS transactions (
    id SERIAL PRIMARY KEY,
    external_id CHAR(36) NOT NULL UNIQUE,
    wallet_id INTEGER NOT NULL REFERENCES wallets(id),
    operation_type_id INTEGER NOT NULL REFERENCES operation_types(id),
    amount DECIMAL NOT NULL,
    event_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS transactions_event_date_key ON transactions USING btree(event_date);
