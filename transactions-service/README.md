## Transactions Service

### Some available commands in Makefile

-   `setup`: Installs some development tools
-   `api-run`: Runs the API
-   `api-image`: Builds the API image
-   `subscriber-run`: Runs the subscriber
-   `subscriber-image`: Builds the subscriber image
-   `fmt`: Formats the code
-   `mock`: Generate the mocks
-   `test`: Runs the unit tests
-   `coverage`: Runs the test coverage measurement
-   `lint`: Runs the go linter
