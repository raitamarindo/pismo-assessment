package postgres

import (
	"database/sql"
	"database/sql/driver"
	"errors"
	"fmt"

	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"

	// CloudSQL Postgres driver - necessary to use CloudSQL Proxy
	_ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/postgres"
	// Local Postgres driver
	_ "github.com/lib/pq"
)

//Client ...
type Client struct {
	db *sql.DB
}

//NewClient instantiates a new Postgres client
func NewClient(connectionString string, maxOpenConns int, dbDriver string) (Client, error) {
	db, err := sql.Open(dbDriver, connectionString)
	if err != nil {
		return Client{}, err
	}

	if err := db.Ping(); err != nil {
		return Client{}, err
	}

	db.SetMaxOpenConns(maxOpenConns)

	return Client{
		db: db,
	}, nil
}

//ExecuteQuery fetch query results from Database
func (c Client) ExecuteQuery(query string, args ...interface{}) (*sql.Rows, error) {
	return c.db.Query(query, args...)
}

// Execute executes a query without returning any rows.
// The args are for any placeholder parameters in the query.
func (c Client) Execute(query string, args ...interface{}) (driver.Result, error) {
	return c.db.Exec(query, args...)
}

// BeginTx ...
func (c Client) BeginTx() (infra.Tx, error) {
	tx, err := c.db.Begin()
	return infra.Tx{Tx: tx}, err
}

// CommitTx ...
func (c Client) CommitTx(tx infra.Tx) error {
	if tx.Tx == nil {
		return errors.New("tx must be opened before commit")
	}
	return tx.Tx.Commit()
}

// RollbackTx ...
func (c Client) RollbackTx(tx infra.Tx) {
	if tx.Tx == nil {
		return
	}
	tx.Tx.Rollback()
}

//ExecuteQueryTx fetch query results from Database with a transaction
func (c Client) ExecuteQueryTx(tx infra.Tx, query string, args ...interface{}) (*sql.Rows, error) {
	if tx.Tx == nil {
		return nil, errors.New("tx must be opened before query")
	}
	return tx.Tx.Query(query, args...)
}

// ExecuteTx executes a query without returning any rows using a transaction.
// The args are for any placeholder parameters in the query.
func (c Client) ExecuteTx(tx infra.Tx, query string, args ...interface{}) (driver.Result, error) {
	if tx.Tx == nil {
		return nil, errors.New("tx must be opened before exec")
	}
	return tx.Tx.Exec(query, args...)
}

//WarmUp ...
func (c Client) WarmUp() error {
	result, err := c.ExecuteQuery("SELECT 1")
	if err != nil {
		return fmt.Errorf("error while trying to warm up the api: %s", err.Error())
	}

	defer result.Close()
	var row int
	if result.Next() {
		if err := result.Scan(&row); err != nil {
			return fmt.Errorf("error while scanning: %s", err.Error())
		}
	}

	return nil
}
