package log

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	"golang.org/x/sync/errgroup"

	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
)

type logValues struct {
	TraceID   string      `json:"trace_id"`
	AccountID string      `json:"account_id"`
	Level     string      `json:"level"`
	Context   string      `json:"context"`
	Message   string      `json:"message"`
	Interval  int         `json:"interval"`
	Count     int         `json:"count"`
	Timestamp time.Time   `json:"timestamp"`
	Data      interface{} `json:"data"`
}

type logLevel int

const (
	logLevelError logLevel = iota + 1
	logLevelInfo
	logLevelDebug
)

var logLevels = map[string]logLevel{
	"ERROR": logLevelError,
	"INFO":  logLevelInfo,
	"DEBUG": logLevelDebug,
}

// Client ...
type Client struct {
	level logLevel
	chLog chan []byte
	g     *errgroup.Group
}

// NewClient ...
func NewClient(level string) Client {
	lvl, ok := logLevels[strings.ToUpper(level)]
	if !ok {
		lvl = 2
	}

	client := Client{level: lvl, chLog: make(chan []byte, 100), g: &errgroup.Group{}}
	client.g.Go(func() error {
		client.logRoutine()
		return nil
	})

	return client
}

// Close closes the asyncronous writing channel and awaits until it finishes writing
func (c Client) Close() {
	close(c.chLog)
	c.g.Wait()
}

// Error ...
func (c Client) Error(options infra.LogOptions, ctx, msg string) {
	c.Errorf(options, ctx, msg)
}

// Errorf ...
func (c Client) Errorf(options infra.LogOptions, ctx, msg string, msgParameters ...interface{}) {
	if c.level >= logLevelError {
		c.log(options, "ERROR", ctx, msg, msgParameters...)
	}
}

// Info ...
func (c Client) Info(options infra.LogOptions, ctx, msg string) {
	c.Infof(options, ctx, msg)
}

// Infof ...
func (c Client) Infof(options infra.LogOptions, ctx, msg string, msgParameters ...interface{}) {
	if c.level >= logLevelInfo {
		c.log(options, "INFO", ctx, msg, msgParameters...)
	}
}

// Debug ...
func (c Client) Debug(options infra.LogOptions, ctx, msg string) {
	c.Debugf(options, ctx, msg)
}

// Debugf ...
func (c Client) Debugf(options infra.LogOptions, ctx, msg string, msgParameters ...interface{}) {
	if c.level >= logLevelDebug {
		c.log(options, "DEBUG", ctx, msg, msgParameters...)
	}
}

func (c Client) log(options infra.LogOptions, lvl, ctx, msg string, msgParameters ...interface{}) {
	if len(msgParameters) > 0 {
		msg = fmt.Sprintf(msg, msgParameters...)
	}

	logMessage, err := json.Marshal(logValues{
		Level:     lvl,
		TraceID:   options.TraceID,
		AccountID: options.AccountID,
		Context:   ctx,
		Message:   msg,
		Interval:  int(options.Interval.Seconds() * 1000),
		Count:     options.Count,
		Timestamp: time.Now().UTC(),
		Data:      options.Data,
	})

	if err != nil {
		logMessage = []byte(msg)
	}

	switch lvl {
	case "ERROR":
		fmt.Fprintln(os.Stderr, string(logMessage))
	case "DEBUG":
		fmt.Println(string(logMessage))
	default:
		c.chLog <- logMessage
	}
}

func (c Client) logRoutine() {
	for msg := range c.chLog {
		fmt.Println(string(msg))
	}
}
