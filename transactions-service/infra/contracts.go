package infra

import (
	"database/sql"
	"database/sql/driver"
	"time"
)

// LogProvider ...
type LogProvider interface {
	// Debug ...
	Debug(options LogOptions, context, msg string)
	// Debugf ...
	Debugf(options LogOptions, context, msg string, msgParameters ...interface{})
	// Info ...
	Info(options LogOptions, context, msg string)
	// Infof ...
	Infof(options LogOptions, context, msg string, msgParameters ...interface{})
	// Error ...
	Error(options LogOptions, context, msg string)
	// Errorf ...
	Errorf(options LogOptions, context, msg string, msgParameters ...interface{})
}

// LogOptions ...
type LogOptions struct {
	TraceID   string
	AccountID string
	Interval  time.Duration
	Count     int
	Data      interface{}
}

//DatabaseClient ...
type DatabaseClient interface {
	Session
	ExecuteQuery(query string, args ...interface{}) (*sql.Rows, error)
	Execute(query string, args ...interface{}) (driver.Result, error)
	ExecuteQueryTx(tx Tx, query string, args ...interface{}) (*sql.Rows, error)
	ExecuteTx(tx Tx, query string, args ...interface{}) (driver.Result, error)
	WarmUp() error
}

//Session ...
type Session interface {
	BeginTx() (Tx, error)
	CommitTx(tx Tx) error
	RollbackTx(tx Tx)
}

//Tx ...
type Tx struct {
	Tx *sql.Tx
}
