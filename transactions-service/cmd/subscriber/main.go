package main

import (
	"context"
	"encoding/json"
	"os"
	"strconv"

	"github.com/streadway/amqp"

	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain/wallets"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra/log"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra/postgres"
)

type config struct {
	port                       string
	logLevel                   string
	databaseURI                string
	databaseMaxOpenConns       int
	messageBrokerURI           string
	accountCreatedTopic        string
	accountCreatedSubscription string
}

func main() {
	config := config{
		port:                       os.Getenv("PORT"),
		logLevel:                   os.Getenv("LOG_LEVEL"),
		databaseURI:                os.Getenv("DATABASE_URI"),
		databaseMaxOpenConns:       1,
		messageBrokerURI:           os.Getenv("MESSAGE_BROKER_URI"),
		accountCreatedTopic:        os.Getenv("ACCOUNT_CREATED_TOPIC"),
		accountCreatedSubscription: os.Getenv("ACCOUNT_CREATED_SUBSCRIPTION"),
	}
	if t, err := strconv.Atoi(os.Getenv("DATABASE_MAX_OPEN_CONNS")); err == nil {
		config.databaseMaxOpenConns = t
	}

	// Logger
	logger := log.NewClient(config.logLevel)
	logger.Info(infra.LogOptions{}, "starting-api", "starting the subscriber...")

	// Database
	database, err := postgres.NewClient(config.databaseURI, config.databaseMaxOpenConns, "postgres")
	if err != nil {
		logger.Error(infra.LogOptions{}, "initializing-database-connection", err.Error())
		os.Exit(1)
	}
	logger.Info(infra.LogOptions{}, "initializing-database-connection", "initialising database connection ...")

	conn, err := amqp.Dial(config.messageBrokerURI)
	if err != nil {
		logger.Error(infra.LogOptions{}, "initializing-message-broker-connection", err.Error())
		os.Exit(1)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		logger.Error(infra.LogOptions{}, "initializing-message-broker-channel", err.Error())
		os.Exit(1)
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		config.accountCreatedTopic, // name
		"fanout",                   // type
		true,                       // durable
		false,                      // auto-deleted
		false,                      // internal
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		logger.Error(infra.LogOptions{}, "declaring-message-broker-topic", err.Error())
		os.Exit(1)
	}

	q, err := ch.QueueDeclare(
		config.accountCreatedSubscription, // name
		true,                              // durable
		false,                             // delete when unused
		false,                             // exclusive
		false,                             // no-wait
		nil,                               // arguments
	)
	if err != nil {
		logger.Error(infra.LogOptions{}, "declaring-message-broker-queue", err.Error())
		os.Exit(1)
	}

	err = ch.QueueBind(
		q.Name,                            // queue name
		config.accountCreatedSubscription, // routing key
		config.accountCreatedTopic,        // exchange
		false,
		nil,
	)
	if err != nil {
		logger.Error(infra.LogOptions{}, "binding-message-broker-topic-with-queue", err.Error())
		os.Exit(1)
	}

	deliveries, err := ch.Consume(
		q.Name,                            // queue
		config.accountCreatedSubscription, // consumer
		false,                             // auto-ack
		false,                             // exclusive
		false,                             // no-local
		false,                             // no-wait
		nil,                               // args
	)
	if err != nil {
		logger.Error(infra.LogOptions{}, "consuming-message-broker-messages", err.Error())
		os.Exit(1)
	}

	forever := make(chan bool)

	// Reposistories
	walletRepo := wallets.NewRepository(database)

	// Services
	walletSvc := wallets.NewService(walletRepo, logger)

	go func() {
		for d := range deliveries {
			var message struct {
				TraceID string                     `json:"trace_id"`
				Data    []domain.WalletCreateInput `json:"data"`
			}
			logger.Infof(infra.LogOptions{}, "receiving-message", "got a message %d bytes", len(d.Body))
			err = json.Unmarshal(d.Body, &message)
			if err != nil {
				logger.Error(infra.LogOptions{}, "message-parsing-failure", err.Error())
				ch.Nack(d.DeliveryTag, false, true)
				continue
			}

			ctx := context.WithValue(context.Background(), domain.ContextKeyTraceID, message.TraceID)
			_, err := walletSvc.CreateMany(ctx, message.Data)
			if err != nil {
				logger.Error(infra.LogOptions{}, "message-processing-failure", err.Error())
				ch.Nack(d.DeliveryTag, false, true)
				continue
			}

			ch.Ack(d.DeliveryTag, false)
		}
	}()

	logger.Info(infra.LogOptions{}, "starting-subscriber", "waiting for events")
	<-forever
}
