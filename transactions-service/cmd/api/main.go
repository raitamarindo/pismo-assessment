package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/google/uuid"
	routing "github.com/jackwhelpton/fasthttp-routing/v2"
	"github.com/jackwhelpton/fasthttp-routing/v2/access"
	"github.com/jackwhelpton/fasthttp-routing/v2/fault"
	"github.com/jackwhelpton/fasthttp-routing/v2/slash"
	"github.com/valyala/fasthttp"
	"github.com/vingarcia/go-adapter"

	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain/operationtypes"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain/transactions"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain/wallets"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra/log"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra/postgres"
)

const (
	// ContextKeyRequestID ...
	ContextKeyRequestID = "request-id"
)

type config struct {
	port                 string
	logLevel             string
	databaseURI          string
	databaseMaxOpenConns int
}

func main() {
	config := config{
		port:                 os.Getenv("PORT"),
		logLevel:             os.Getenv("LOG_LEVEL"),
		databaseURI:          os.Getenv("DATABASE_URI"),
		databaseMaxOpenConns: 1,
	}
	if t, err := strconv.Atoi(os.Getenv("DATABASE_MAX_OPEN_CONNS")); err == nil {
		config.databaseMaxOpenConns = t
	}

	// Logger
	logger := log.NewClient(config.logLevel)
	logger.Info(infra.LogOptions{}, "starting-api", "starting the API...")

	// Database
	database, err := postgres.NewClient(config.databaseURI, config.databaseMaxOpenConns, "postgres")
	if err != nil {
		logger.Error(infra.LogOptions{}, "initializing-database-connection", err.Error())
		os.Exit(1)
	}
	logger.Info(infra.LogOptions{}, "initializing-database-connection", "initialising database connection ...")

	// Reposistories
	operationTypeRepo := operationtypes.NewRepository(database)
	walletRepo := wallets.NewRepository(database)
	transactionRepo := transactions.NewRepository(database)

	// Services
	transactionSvc := transactions.NewService(transactionRepo, walletRepo, operationTypeRepo, logger)

	router := routing.New()
	router.Use(
		requestIdentifier(),
		slash.Remover(http.StatusMovedPermanently),
		access.CustomLogger(requestLogger(logger)),
		fault.ErrorHandler(nil, handleRequestError(logger)),
	)
	api := router.Group("/transactions")

	api.Get("/ping", func(ctx *routing.Context) error { ctx.SetBodyString("pong"); return nil })
	api.Post("", adapter.Adapt(func(ctx *routing.Context, input struct {
		Body domain.TransactionCreateInput `content-type:"application/json"`
	}) error {
		requestID, _ := ctx.Value(ContextKeyRequestID).(string)
		output, err := transactionSvc.Create(context.WithValue(ctx, domain.ContextKeyTraceID, requestID), input.Body)
		if err != nil {
			return err
		}

		return responseJSON(ctx, output)
	}))

	logger.Infof(infra.LogOptions{}, "listen-http", "server listening at %s", config.port)
	err = fasthttp.ListenAndServe(":"+config.port, router.HandleRequest)
	if err != nil {
		logger.Error(infra.LogOptions{}, "listen-http", err.Error())
	}
}

func responseJSON(ctx *routing.Context, payload interface{}) error {
	data, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	ctx.SetContentType("application/json")
	ctx.SetBody(data)

	return nil
}

func requestIdentifier() func(ctx *routing.Context) error {
	return func(ctx *routing.Context) error {
		requestID := string(ctx.Request.Header.Peek("X-Pismo-Trace-ID"))
		if requestID == "" {
			requestID = uuid.New().String()
		}
		ctx.SetUserValue(ContextKeyRequestID, requestID)

		return nil
	}
}

func requestLogger(logger infra.LogProvider) func(ctx *fasthttp.RequestCtx, elapsed float64) {
	return func(ctx *fasthttp.RequestCtx, elapsed float64) {
		requestID, _ := ctx.UserValue(ContextKeyRequestID).(string)
		ip := access.GetClientIP(ctx)
		method := string(ctx.Request.Header.Method())
		uri := string(ctx.RequestURI())
		status := strconv.Itoa(ctx.Response.StatusCode())
		logOptions := infra.LogOptions{
			TraceID:  requestID,
			Interval: time.Duration(elapsed) * time.Millisecond,
			Data: map[string]string{
				"remote_ip": ip,
				"method":    method,
				"uri":       uri,
				"status":    status,
			},
		}
		logger.Infof(logOptions, "request-tracking", `[%s] [%.3fms] %s %s %s %d`, ip, elapsed, method, uri, status, len(ctx.Response.Body()))
	}
}

func handleRequestError(logger infra.LogProvider) func(ctx *routing.Context, err error) error {
	return func(ctx *routing.Context, err error) error {
		requestID, _ := ctx.UserValue(ContextKeyRequestID).(string)
		logger.Error(infra.LogOptions{TraceID: requestID}, "request-error", err.Error())

		ctx.SetContentTypeBytes([]byte("application/json"))
		var serializedErr string
		switch err.(type) {
		case routing.HTTPError:
			var e routing.HTTPError
			errors.As(err, &e)
			serializedErr := fmt.Sprintf(`{"error":"%s"}`, err.Error())
			return routing.NewHTTPError(e.StatusCode(), serializedErr)
		case domain.Error:
			var e domain.Error
			errors.As(err, &e)
			serializedErr := fmt.Sprintf(`{"error":"%s","code":"%s"}`, e.Error(), e.Code())
			if e.Code() == domain.ErrorCodeNotFound {
				return routing.NewHTTPError(http.StatusNotFound, serializedErr)
			}
			return routing.NewHTTPError(http.StatusBadRequest, serializedErr)
		default:
			serializedErr = fmt.Sprintf(`{"error":"internal","request_id":"%s"}`, requestID)
		}

		return routing.NewHTTPError(http.StatusInternalServerError, serializedErr)
	}
}
