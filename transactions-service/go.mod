module gitlab.com/raitamarindo/pismo-assessment/transactions-service

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/GoogleCloudPlatform/cloudsql-proxy v1.18.0
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/golang/mock v1.4.4
	github.com/google/uuid v1.1.2
	github.com/jackwhelpton/fasthttp-routing/v2 v2.0.0
	github.com/klauspost/compress v1.11.1 // indirect
	github.com/lib/pq v1.8.0
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.4.0
	github.com/valyala/fasthttp v1.16.0
	github.com/vingarcia/go-adapter v0.0.0-20201001162140-1f6809bd95e3
	golang.org/x/sync v0.0.0-20200930132711-30421366ff76
)
