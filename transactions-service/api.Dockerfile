FROM golang:1.15-alpine3.12 as builder

ENV CGO_ENABLED=0

WORKDIR /app

COPY . .
RUN go mod download
RUN go build -o /api -mod=readonly ./cmd/api/main.go

FROM alpine

COPY --from=builder /api /api

ENTRYPOINT [ "/api" ]