package operationtypes

import (
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
)

// Repository ...
type Repository struct {
	databaseClient infra.DatabaseClient
}

// NewRepository ...
func NewRepository(databaseClient infra.DatabaseClient) Repository {
	return Repository{
		databaseClient: databaseClient,
	}
}

// GetByExternalID ...
func (r Repository) GetByExternalID(externalID string) (*domain.OperationType, error) {
	query := `
	SELECT
		id,
		external_id,
		description,
		is_negative
	FROM
		operation_types
	WHERE
		external_id = $1`

	result, err := r.databaseClient.ExecuteQuery(query, externalID)
	if err != nil {
		return nil, err
	}

	var operationType *domain.OperationType
	if result.Next() {
		var o domain.OperationType
		err = result.Scan(&o.ID, &o.ExternalID, &o.Description, &o.Negative)
		if err != nil {
			return nil, err
		}
		operationType = &o
	}
	if result.Err() != nil {
		return nil, result.Err()
	}

	return operationType, nil
}
