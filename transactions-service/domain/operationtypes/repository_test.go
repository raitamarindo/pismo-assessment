package operationtypes

import (
	"database/sql"
	"errors"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/mocks"
)

func TestRepository_GetByExternalID(t *testing.T) {
	var databaseClient *mocks.MockDatabaseClient
	var sqlMock sqlmock.Sqlmock

	withMock := func(runner func(t *testing.T, r Repository)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			var db *sql.DB
			var err error

			db, sqlMock, err = sqlmock.New()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			databaseClient = mocks.NewMockDatabaseClient(ctrl)

			databaseClient.EXPECT().
				ExecuteQuery(gomock.Any(), gomock.Any()).
				DoAndReturn(func(query string, args ...interface{}) (*sql.Rows, error) {
					return db.Query(query, args...)
				}).
				AnyTimes()

			runner(t, NewRepository(databaseClient))
		}
	}

	query := `
	SELECT
		id,
		external_id,
		description,
		is_negative
	FROM
		operation_types
	WHERE
		external_id = $1`

	t.Run(
		"should return an error when database fails on fetching row",
		withMock(func(t *testing.T, r Repository) {
			sqlMock.ExpectQuery(regexp.QuoteMeta(query)).
				WithArgs("uuid-1").
				WillReturnError(errors.New("any error"))

			_, err := r.GetByExternalID("uuid-1")

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should return nil when database returns empty rows",
		withMock(func(t *testing.T, r Repository) {
			rows := sqlMock.NewRows([]string{"id", "external_id", "description", "is_negative"})

			sqlMock.ExpectQuery(regexp.QuoteMeta(query)).
				WithArgs("uuid-1").
				WillReturnRows(rows)

			operationType, err := r.GetByExternalID("uuid-1")

			assert.Nil(t, err)
			assert.Nil(t, operationType)
		}),
	)

	t.Run(
		"should map row to operation type when database returns one row",
		withMock(func(t *testing.T, r Repository) {
			rows := sqlMock.NewRows([]string{"id", "external_id", "description", "is_negative"})
			rows.AddRow(1, "uuid-1", "An operation type", true)

			sqlMock.ExpectQuery(regexp.QuoteMeta(query)).
				WithArgs("uuid-1").
				WillReturnRows(rows)

			operationType, err := r.GetByExternalID("uuid-1")

			assert.Nil(t, err)
			assert.Equal(t, &domain.OperationType{
				ID:          1,
				ExternalID:  "uuid-1",
				Description: "An operation type",
				Negative:    true,
			}, operationType)
		}),
	)
}
