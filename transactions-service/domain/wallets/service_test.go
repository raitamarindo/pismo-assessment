package wallets

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/mocks"
)

func TestService_CreateMany(t *testing.T) {
	var walletRepo *mocks.MockWalletRepository

	withMock := func(runner func(t *testing.T, s Service)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			walletRepo = mocks.NewMockWalletRepository(ctrl)
			logger := mocks.NewMockLogProvider(ctrl)

			logger.EXPECT().Error(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()
			logger.EXPECT().Infof(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()

			runner(t, NewService(walletRepo, logger))
		}
	}

	t.Run(
		"should return an error when account id is empty",
		withMock(func(t *testing.T, s Service) {
			_, err := s.CreateMany(context.TODO(), []domain.WalletCreateInput{{}})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeMissingAccountID, e.Code())
		}),
	)

	t.Run(
		"should return an error when repository fails on create wallets",
		withMock(func(t *testing.T, s Service) {
			input := []domain.WalletCreateInput{{
				AccountID: "ac-1",
			}}

			walletRepo.EXPECT().
				Insert(gomock.Any()).
				Return([]domain.Wallet{}, errors.New("any error"))

			_, err := s.CreateMany(context.TODO(), input)

			assert.EqualError(t, err, "failed on trying to create wallets")
		}),
	)

	t.Run(
		"should return created wallets when on error occur",
		withMock(func(t *testing.T, s Service) {
			input := []domain.WalletCreateInput{{
				AccountID: "ac-1",
			}}
			expected := []domain.WalletOutput{{
				ID:        "wl-1",
				AccountID: input[0].AccountID,
			}}

			walletRepo.EXPECT().
				Insert(gomock.Any()).
				Return([]domain.Wallet{{
					ID:         1,
					ExternalID: expected[0].ID,
					AccountID:  expected[0].AccountID,
				}}, nil)

			output, err := s.CreateMany(context.TODO(), input)

			assert.Nil(t, err)
			assert.Equal(t, expected, output)
		}),
	)
}
