package wallets

import (
	"context"
	"errors"
	"time"

	"github.com/google/uuid"

	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
)

// Service ...
type Service struct {
	walletRepo domain.WalletRepository
	logger     infra.LogProvider
}

// NewService ...
func NewService(walletRepo domain.WalletRepository, logger infra.LogProvider) Service {
	return Service{
		walletRepo: walletRepo,
		logger:     logger,
	}
}

// CreateMany ...
func (s Service) CreateMany(ctx context.Context, inputs []domain.WalletCreateInput) ([]domain.WalletOutput, error) {
	traceID, _ := ctx.Value(domain.ContextKeyTraceID).(string)
	logOptions := infra.LogOptions{
		TraceID: traceID,
	}
	empty := []domain.WalletOutput{}

	if len(inputs) == 0 {
		s.logger.Info(logOptions, "create-many-empty-wallet-create-input", "empty wallets list")
		return empty, nil
	}

	wallets := []domain.Wallet{}
	accountIDs := []string{}
	for _, input := range inputs {
		if input.AccountID != "" {
			wallets = append(wallets, domain.Wallet{
				ExternalID: uuid.New().String(),
				AccountID:  input.AccountID,
			})
			accountIDs = append(accountIDs, input.AccountID)
			continue
		}
		msg := "wallet account id can not be empty"
		s.logger.Error(logOptions, "create-many-validating-account-id", msg)
		return empty, domain.NewError(msg, domain.ErrorCodeMissingAccountID)
	}
	logOptions.Data = map[string]interface{}{
		"account_ids": accountIDs,
	}

	var err error
	t := time.Now()
	wallets, err = s.walletRepo.Insert(wallets...)
	if err != nil {
		s.logger.Error(logOptions, "create-many-inserting-wallets", err.Error())
		return empty, errors.New("failed on trying to create wallets")
	}
	logOptions.Interval = time.Since(t)
	logOptions.Count = len(wallets)
	s.logger.Infof(logOptions, "create-many-inserting-wallets", "created %d wallets", len(wallets))

	outputs := []domain.WalletOutput{}
	for _, wallet := range wallets {
		outputs = append(outputs, domain.WalletOutput{
			ID:        wallet.ExternalID,
			AccountID: wallet.AccountID,
		})
	}

	return outputs, nil
}
