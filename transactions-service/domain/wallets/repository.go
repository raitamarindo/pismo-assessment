package wallets

import (
	"fmt"
	"strings"

	"github.com/lib/pq"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
)

// Repository ...
type Repository struct {
	database infra.DatabaseClient
}

// NewRepository ...
func NewRepository(database infra.DatabaseClient) Repository {
	return Repository{
		database: database,
	}
}

// GetByAccountID ...
func (r Repository) GetByAccountID(accountID string) (*domain.Wallet, error) {
	query := `
	SELECT
		id,
		account_id
	FROM
		wallets
	WHERE
		account_id = $1`

	result, err := r.database.ExecuteQuery(query, accountID)
	if err != nil {
		return nil, err
	}
	defer result.Close()

	var wallet *domain.Wallet
	if result.Next() {
		var w domain.Wallet
		err = result.Scan(&w.ID, &w.AccountID)
		wallet = &w
	}
	if err != nil {
		return nil, err
	}
	if result.Err() != nil {
		return nil, result.Err()
	}

	return wallet, nil
}

// Insert ...
func (r Repository) Insert(wallets ...domain.Wallet) ([]domain.Wallet, error) {
	empty := []domain.Wallet{}
	tx, err := r.database.BeginTx()
	if err != nil {
		return empty, err
	}

	wallets, err = r.insert(wallets, tx)
	if err != nil {
		r.database.RollbackTx(tx)
		return empty, err
	}
	r.database.CommitTx(tx)

	return wallets, nil
}

// InsertTx ...
func (r Repository) InsertTx(wallets []domain.Wallet, tx infra.Tx) ([]domain.Wallet, error) {
	return r.insert(wallets, tx)
}

func (r Repository) insert(wallets []domain.Wallet, tx infra.Tx) ([]domain.Wallet, error) {
	empty := []domain.Wallet{}
	sql := "INSERT INTO wallets(external_id, account_id) VALUES"
	params := []interface{}{}
	values := []string{}

	externalIDs := []string{}
	for _, wallet := range wallets {
		params = append(params, wallet.ExternalID, wallet.AccountID)
		values = append(values, fmt.Sprintf("($%d, $%d)", len(params)-1, len(params)))
		externalIDs = append(externalIDs, wallet.ExternalID)
	}

	sql += strings.Join(values, ",")
	_, err := r.database.ExecuteTx(tx, sql, params...)
	if err != nil {
		return empty, err
	}

	result, err := r.database.ExecuteQueryTx(tx, "SELECT id, external_id FROM transactions WHERE external_id = ANY($1)", pq.Array(externalIDs))
	if err != nil {
		return empty, err
	}
	defer result.Close()

	idMap := map[string]uint{}
	for result.Next() {
		var id uint
		var externalID string
		if err = result.Scan(&id, &externalID); err != nil {
			return empty, err
		}
		idMap[externalID] = id
	}
	if result.Err() != nil {
		return empty, result.Err()
	}

	for i, wallet := range wallets {
		wallets[i].ID = idMap[wallet.ExternalID]
	}

	return wallets, nil
}
