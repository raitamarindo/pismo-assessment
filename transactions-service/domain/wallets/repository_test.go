package wallets

import (
	"database/sql"
	"errors"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang/mock/gomock"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/mocks"
)

func TestRepository_GetByAccountID(t *testing.T) {
	var databaseClient *mocks.MockDatabaseClient
	var sqlMock sqlmock.Sqlmock

	withMock := func(runner func(t *testing.T, r Repository)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			var db *sql.DB
			var err error

			db, sqlMock, err = sqlmock.New()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			databaseClient = mocks.NewMockDatabaseClient(ctrl)

			databaseClient.EXPECT().
				ExecuteQuery(gomock.Any(), gomock.Any()).
				DoAndReturn(func(query string, args ...interface{}) (*sql.Rows, error) {
					return db.Query(query, args...)
				}).
				AnyTimes()

			runner(t, NewRepository(databaseClient))
		}
	}

	query := `
	SELECT
		id,
		account_id
	FROM
		wallets
	WHERE
		account_id = $1`

	t.Run(
		"should return an error when database fails on fetching row",
		withMock(func(t *testing.T, r Repository) {
			sqlMock.ExpectQuery(regexp.QuoteMeta(query)).
				WithArgs("wl-1").
				WillReturnError(errors.New("any error"))

			_, err := r.GetByAccountID("wl-1")

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should return nil when database returns empty rows",
		withMock(func(t *testing.T, r Repository) {
			rows := sqlMock.NewRows([]string{"id", "account_id"})

			sqlMock.ExpectQuery(regexp.QuoteMeta(query)).
				WithArgs("wl-1").
				WillReturnRows(rows)

			wallet, err := r.GetByAccountID("wl-1")

			assert.Nil(t, err)
			assert.Nil(t, wallet)
		}),
	)

	t.Run(
		"should map row to operation type when database returns one row",
		withMock(func(t *testing.T, r Repository) {
			rows := sqlMock.NewRows([]string{"id", "account_id"})
			rows.AddRow(1, "wl-1")

			sqlMock.ExpectQuery(regexp.QuoteMeta(query)).
				WithArgs("wl-1").
				WillReturnRows(rows)

			wallet, err := r.GetByAccountID("wl-1")

			assert.Nil(t, err)
			assert.Equal(t, &domain.Wallet{
				ID:        1,
				AccountID: "wl-1",
			}, wallet)
		}),
	)
}

func TestRepository_Insert(t *testing.T) {
	var databaseClient *mocks.MockDatabaseClient
	var sqlMock sqlmock.Sqlmock

	withMock := func(runner func(t *testing.T, r Repository)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			var db *sql.DB
			var err error

			db, sqlMock, err = sqlmock.New()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			databaseClient = mocks.NewMockDatabaseClient(ctrl)

			databaseClient.EXPECT().
				ExecuteTx(gomock.Any(), gomock.Any(), gomock.Any()).
				DoAndReturn(func(tx infra.Tx, query string, args ...interface{}) (sql.Result, error) {
					return db.Exec(query, args...)
				}).
				AnyTimes()
			databaseClient.EXPECT().
				ExecuteQueryTx(gomock.Any(), gomock.Any(), gomock.Any()).
				DoAndReturn(func(tx infra.Tx, query string, args ...interface{}) (*sql.Rows, error) {
					return db.Query(query, args...)
				}).
				AnyTimes()

			runner(t, NewRepository(databaseClient))
		}
	}

	insertQuery := "INSERT INTO wallets(external_id, account_id) VALUES($1, $2)"
	selectQuery := "SELECT id, external_id FROM transactions WHERE external_id = ANY($1)"

	t.Run(
		"should return an error when session manager fails to begin transaction",
		withMock(func(t *testing.T, r Repository) {
			databaseClient.EXPECT().BeginTx().Return(infra.Tx{}, errors.New("any error"))

			_, err := r.Insert(domain.Wallet{})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should return an error when database fails on inserting row",
		withMock(func(t *testing.T, r Repository) {
			tx := infra.Tx{
				Tx: &sql.Tx{},
			}

			databaseClient.EXPECT().BeginTx().Return(tx, nil)
			sqlMock.ExpectExec(regexp.QuoteMeta(insertQuery)).
				WillReturnError(errors.New("any error"))
			databaseClient.EXPECT().RollbackTx(tx)

			_, err := r.Insert(domain.Wallet{
				ExternalID: "wl-1",
				AccountID:  "ac-1",
			})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should return an error when database fails on fetching row id",
		withMock(func(t *testing.T, r Repository) {
			tx := infra.Tx{}

			databaseClient.EXPECT().BeginTx().Return(tx, nil)
			sqlMock.ExpectExec(regexp.QuoteMeta(insertQuery)).
				WillReturnResult(sqlmock.NewResult(1, 1))
			sqlMock.ExpectQuery(regexp.QuoteMeta(selectQuery)).
				WithArgs(pq.Array([]string{"wl-1"})).
				WillReturnError(errors.New("any error"))
			databaseClient.EXPECT().RollbackTx(tx)

			_, err := r.Insert(domain.Wallet{
				ExternalID: "wl-1",
				AccountID:  "ac-1",
			})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should commit and map returned row to an wallet when database fetches the created wallet id",
		withMock(func(t *testing.T, r Repository) {
			tx := infra.Tx{
				Tx: &sql.Tx{},
			}
			rows := sqlmock.NewRows([]string{"id", "external_id"})
			rows.AddRow(1, "wl-1")

			databaseClient.EXPECT().BeginTx().Return(tx, nil)
			sqlMock.ExpectExec(regexp.QuoteMeta(insertQuery)).
				WillReturnResult(sqlmock.NewResult(1, 1))
			sqlMock.ExpectQuery(regexp.QuoteMeta(selectQuery)).
				WithArgs(pq.Array([]string{"wl-1"})).
				WillReturnRows(rows)
			databaseClient.EXPECT().CommitTx(tx).Return(nil)

			wallets, err := r.Insert(domain.Wallet{
				ExternalID: "wl-1",
				AccountID:  "ac-1",
			})

			assert.Nil(t, err)
			assert.Equal(t, uint(1), wallets[0].ID)
			assert.Equal(t, "wl-1", wallets[0].ExternalID)
			assert.Equal(t, "ac-1", wallets[0].AccountID)
		}),
	)
}
