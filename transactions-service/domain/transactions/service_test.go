package transactions

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/mocks"
)

func TestService_Create(t *testing.T) {
	var transactionRepo *mocks.MockTransactionRepository
	var walletRepo *mocks.MockWalletRepository
	var operationTypeRepo *mocks.MockOperationTypeRepository

	withMock := func(runner func(t *testing.T, s Service)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			transactionRepo = mocks.NewMockTransactionRepository(ctrl)
			walletRepo = mocks.NewMockWalletRepository(ctrl)
			operationTypeRepo = mocks.NewMockOperationTypeRepository(ctrl)
			logger := mocks.NewMockLogProvider(ctrl)

			logger.EXPECT().Error(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()
			logger.EXPECT().Infof(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()

			runner(t, NewService(transactionRepo, walletRepo, operationTypeRepo, logger))
		}
	}

	t.Run(
		"should return an error when account id is empty",
		withMock(func(t *testing.T, s Service) {
			_, err := s.Create(context.TODO(), domain.TransactionCreateInput{
				OperationTypeID: "op-1",
				Amount:          123.45,
			})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeMissingAccountID, e.Code())
		}),
	)

	t.Run(
		"should return an error when operation type id is empty",
		withMock(func(t *testing.T, s Service) {
			_, err := s.Create(context.TODO(), domain.TransactionCreateInput{
				AccountID: "ac-1",
				Amount:    123.45,
			})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeMissingOperationTypeID, e.Code())
		}),
	)

	t.Run(
		"should return an error when amount is empty",
		withMock(func(t *testing.T, s Service) {
			_, err := s.Create(context.TODO(), domain.TransactionCreateInput{
				AccountID:       "ac-1",
				OperationTypeID: "op-1",
			})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeMissingAmount, e.Code())
		}),
	)

	t.Run(
		"should return an error when repository fails on get account wallet",
		withMock(func(t *testing.T, s Service) {
			input := domain.TransactionCreateInput{
				AccountID:       "ac-1",
				OperationTypeID: "op-1",
				Amount:          123.45,
			}

			operationTypeRepo.EXPECT().GetByExternalID(gomock.Any()).Return(&domain.OperationType{}, nil).AnyTimes()
			walletRepo.EXPECT().
				GetByAccountID(input.AccountID).
				Return(nil, errors.New("any error"))

			_, err := s.Create(context.TODO(), input)

			assert.EqualError(t, err, "failed on trying to get account wallet")
		}),
	)

	t.Run(
		"should return an error when repository fails on get operation type",
		withMock(func(t *testing.T, s Service) {
			input := domain.TransactionCreateInput{
				AccountID:       "ac-1",
				OperationTypeID: "op-1",
				Amount:          123.45,
			}

			walletRepo.EXPECT().GetByAccountID(gomock.Any()).Return(&domain.Wallet{}, nil).AnyTimes()
			operationTypeRepo.EXPECT().
				GetByExternalID(input.OperationTypeID).
				Return(nil, errors.New("any error"))

			_, err := s.Create(context.TODO(), input)

			assert.EqualError(t, err, "failed on trying to get operation type")
		}),
	)

	t.Run(
		"should return a not found error when repository return nil wallet with no error",
		withMock(func(t *testing.T, s Service) {
			input := domain.TransactionCreateInput{
				AccountID:       "ac-1",
				OperationTypeID: "op-1",
				Amount:          123.45,
			}

			operationTypeRepo.EXPECT().GetByExternalID(gomock.Any()).Return(&domain.OperationType{}, nil).AnyTimes()
			walletRepo.EXPECT().
				GetByAccountID(input.AccountID).
				Return(nil, nil)

			_, err := s.Create(context.TODO(), input)

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeNotFound, e.Code())
		}),
	)

	t.Run(
		"should return a not found error when repository return nil operation type with no error",
		withMock(func(t *testing.T, s Service) {
			input := domain.TransactionCreateInput{
				AccountID:       "ac-1",
				OperationTypeID: "op-1",
				Amount:          123.45,
			}

			walletRepo.EXPECT().GetByAccountID(gomock.Any()).Return(&domain.Wallet{}, nil).AnyTimes()
			operationTypeRepo.EXPECT().
				GetByExternalID(input.OperationTypeID).
				Return(nil, nil)

			_, err := s.Create(context.TODO(), input)

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeNotFound, e.Code())
		}),
	)

	t.Run(
		"should return an invalid amount error when the amount is positive but the operation type is negative",
		withMock(func(t *testing.T, s Service) {
			input := domain.TransactionCreateInput{
				AccountID:       "ac-1",
				OperationTypeID: "op-1",
				Amount:          123.45,
			}

			walletRepo.EXPECT().GetByAccountID(gomock.Any()).Return(&domain.Wallet{}, nil).AnyTimes()
			operationTypeRepo.EXPECT().
				GetByExternalID(input.OperationTypeID).
				Return(&domain.OperationType{
					ID:          1,
					ExternalID:  "op-1",
					Description: "An operation type",
					Negative:    true,
				}, nil)

			_, err := s.Create(context.TODO(), input)

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeInvalidAmount, e.Code())
		}),
	)

	t.Run(
		"should return an invalid amount error when the amount is negative but the operation type is positive",
		withMock(func(t *testing.T, s Service) {
			input := domain.TransactionCreateInput{
				AccountID:       "ac-1",
				OperationTypeID: "op-1",
				Amount:          -123.45,
			}

			walletRepo.EXPECT().GetByAccountID(gomock.Any()).Return(&domain.Wallet{}, nil).AnyTimes()
			operationTypeRepo.EXPECT().
				GetByExternalID(input.OperationTypeID).
				Return(&domain.OperationType{
					ID:          1,
					ExternalID:  "op-1",
					Description: "An operation type",
					Negative:    false,
				}, nil)

			_, err := s.Create(context.TODO(), input)

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeInvalidAmount, e.Code())
		}),
	)

	t.Run(
		"should return an error when repository fails on create transaction",
		withMock(func(t *testing.T, s Service) {
			input := domain.TransactionCreateInput{
				AccountID:       "ac-1",
				OperationTypeID: "op-1",
				Amount:          123.45,
			}

			walletRepo.EXPECT().GetByAccountID(gomock.Any()).Return(&domain.Wallet{}, nil).AnyTimes()
			operationTypeRepo.EXPECT().GetByExternalID(gomock.Any()).Return(&domain.OperationType{}, nil).AnyTimes()
			transactionRepo.EXPECT().
				Insert(gomock.Any()).
				Return(domain.Transaction{}, errors.New("any error"))

			_, err := s.Create(context.TODO(), input)

			assert.EqualError(t, err, "failed on trying to create transaction")
		}),
	)

	t.Run(
		"should return created transaction when on error occur",
		withMock(func(t *testing.T, s Service) {
			input := domain.TransactionCreateInput{
				AccountID:       "ac-1",
				OperationTypeID: "op-1",
				Amount:          123.45,
			}
			expected := domain.TransactionOutput{
				ID:              "tr-1",
				AccountID:       input.AccountID,
				OperationTypeID: input.OperationTypeID,
				Amount:          input.Amount,
				EventDate:       time.Now(),
			}

			walletRepo.EXPECT().GetByAccountID(gomock.Any()).Return(&domain.Wallet{}, nil).AnyTimes()
			operationTypeRepo.EXPECT().GetByExternalID(gomock.Any()).Return(&domain.OperationType{}, nil).AnyTimes()
			transactionRepo.EXPECT().
				Insert(gomock.Any()).
				Return(domain.Transaction{
					ID:         1,
					ExternalID: expected.ID,
					Amount:     expected.Amount,
					EventDate:  expected.EventDate,
					Wallet: domain.Wallet{
						ID:        1,
						AccountID: expected.AccountID,
					},
					OperationType: domain.OperationType{
						ID:         1,
						ExternalID: expected.OperationTypeID,
					},
				}, nil)

			output, err := s.Create(context.TODO(), input)

			assert.Nil(t, err)
			assert.Equal(t, expected, output)
		}),
	)
}
