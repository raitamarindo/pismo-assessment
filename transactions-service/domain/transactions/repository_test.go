package transactions

import (
	"database/sql"
	"errors"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/mocks"
)

func TestRepository_Insert(t *testing.T) {
	var databaseClient *mocks.MockDatabaseClient
	var sqlMock sqlmock.Sqlmock

	withMock := func(runner func(t *testing.T, r Repository)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			var db *sql.DB
			var err error

			db, sqlMock, err = sqlmock.New()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			databaseClient = mocks.NewMockDatabaseClient(ctrl)

			databaseClient.EXPECT().
				ExecuteTx(gomock.Any(), gomock.Any(), gomock.Any()).
				DoAndReturn(func(tx infra.Tx, query string, args ...interface{}) (sql.Result, error) {
					return db.Exec(query, args...)
				}).
				AnyTimes()
			databaseClient.EXPECT().
				ExecuteQueryTx(gomock.Any(), gomock.Any(), gomock.Any()).
				DoAndReturn(func(tx infra.Tx, query string, args ...interface{}) (*sql.Rows, error) {
					return db.Query(query, args...)
				}).
				AnyTimes()

			runner(t, NewRepository(databaseClient))
		}
	}

	insertQuery := "INSERT INTO transactions(external_id, amount, event_date, wallet_id, operation_type_id) VALUES($1, $2, $3, $4, $5)"
	selectQuery := "SELECT id FROM transactions WHERE external_id = $1"

	t.Run(
		"should return an error when session manager fails to begin transaction",
		withMock(func(t *testing.T, r Repository) {
			databaseClient.EXPECT().BeginTx().Return(infra.Tx{}, errors.New("any error"))

			_, err := r.Insert(domain.Transaction{})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should return an error when database fails on inserting row",
		withMock(func(t *testing.T, r Repository) {
			tx := infra.Tx{
				Tx: &sql.Tx{},
			}
			now := time.Now()

			databaseClient.EXPECT().BeginTx().Return(tx, nil)
			sqlMock.ExpectExec(regexp.QuoteMeta(insertQuery)).
				WillReturnError(errors.New("any error"))
			databaseClient.EXPECT().RollbackTx(tx)

			_, err := r.Insert(domain.Transaction{
				ExternalID: "uuid-1",
				Amount:     123.45,
				EventDate:  now,
				Wallet: domain.Wallet{
					ID: 1,
				},
				OperationType: domain.OperationType{
					ID: 2,
				},
			})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should return an error when database fails on fetching row id",
		withMock(func(t *testing.T, r Repository) {
			tx := infra.Tx{}
			now := time.Now()

			databaseClient.EXPECT().BeginTx().Return(tx, nil)
			sqlMock.ExpectExec(regexp.QuoteMeta(insertQuery)).
				WillReturnResult(sqlmock.NewResult(1, 1))
			sqlMock.ExpectQuery(regexp.QuoteMeta(selectQuery)).
				WithArgs("uuid-1").
				WillReturnError(errors.New("any error"))
			databaseClient.EXPECT().RollbackTx(tx)

			_, err := r.Insert(domain.Transaction{
				ExternalID: "uuid-1",
				Amount:     123.45,
				EventDate:  now,
				Wallet: domain.Wallet{
					ID: 1,
				},
				OperationType: domain.OperationType{
					ID: 2,
				},
			})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should commit and map returned row to an account when database fetches the created account id",
		withMock(func(t *testing.T, r Repository) {
			tx := infra.Tx{
				Tx: &sql.Tx{},
			}
			now := time.Now()
			rows := sqlmock.NewRows([]string{"id"})
			rows.AddRow(1)

			databaseClient.EXPECT().BeginTx().Return(tx, nil)
			sqlMock.ExpectExec(regexp.QuoteMeta(insertQuery)).
				WillReturnResult(sqlmock.NewResult(1, 1))
			sqlMock.ExpectQuery(regexp.QuoteMeta(selectQuery)).
				WithArgs("uuid-1").
				WillReturnRows(rows)
			databaseClient.EXPECT().CommitTx(tx).Return(nil)

			transaction, err := r.Insert(domain.Transaction{
				ExternalID: "uuid-1",
				Amount:     123.45,
				EventDate:  now,
				Wallet: domain.Wallet{
					ID: 1,
				},
				OperationType: domain.OperationType{
					ID: 2,
				},
			})

			assert.Nil(t, err)
			assert.Equal(t, uint(1), transaction.ID)
			assert.Equal(t, "uuid-1", transaction.ExternalID)
			assert.Equal(t, float32(123.45), transaction.Amount)
			assert.Equal(t, now, transaction.EventDate)
			assert.Equal(t, uint(1), transaction.Wallet.ID)
			assert.Equal(t, uint(2), transaction.OperationType.ID)
		}),
	)
}
