package transactions

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
	"golang.org/x/sync/errgroup"
)

// Service ...
type Service struct {
	transactionRepo   domain.TransactionRepository
	walletRepo        domain.WalletRepository
	operationTypeRepo domain.OperationTypeRepository
	logger            infra.LogProvider
}

// NewService ...
func NewService(
	transactionRepo domain.TransactionRepository,
	walletRepo domain.WalletRepository,
	operationTypeRepo domain.OperationTypeRepository,
	logger infra.LogProvider,
) Service {
	return Service{
		transactionRepo:   transactionRepo,
		walletRepo:        walletRepo,
		operationTypeRepo: operationTypeRepo,
		logger:            logger,
	}
}

// Create ...
func (s Service) Create(ctx context.Context, input domain.TransactionCreateInput) (domain.TransactionOutput, error) {
	traceID, _ := ctx.Value(domain.ContextKeyTraceID).(string)
	logOptions := infra.LogOptions{
		TraceID:   traceID,
		AccountID: input.AccountID,
		Data: map[string]interface{}{
			"operation_type_id": input.OperationTypeID,
		},
	}
	empty := domain.TransactionOutput{}

	if input.AccountID == "" {
		msg := "account id can not be empty"
		s.logger.Error(logOptions, "create-validating-account-id", msg)
		return empty, domain.NewError(msg, domain.ErrorCodeMissingAccountID)
	}

	if input.OperationTypeID == "" {
		msg := "operation type id can not be empty"
		s.logger.Error(logOptions, "create-validating-operation-type-id", msg)
		return empty, domain.NewError(msg, domain.ErrorCodeMissingOperationTypeID)
	}

	if input.Amount == 0 {
		msg := "amount can not be zero"
		s.logger.Error(logOptions, "create-validating-amount", msg)
		return empty, domain.NewError(msg, domain.ErrorCodeMissingAmount)
	}

	transaction := domain.Transaction{
		ExternalID: uuid.New().String(),
		Amount:     input.Amount,
		EventDate:  time.Now(),
	}
	g, ctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		l := logOptions
		t := time.Now()
		wallet, err := s.walletRepo.GetByAccountID(input.AccountID)
		if err != nil {
			s.logger.Error(l, "create-getting-account-wallet", err.Error())
			return errors.New("failed on trying to get account wallet")
		}
		if wallet == nil {
			msg := fmt.Sprintf("the wallet for account %s was not found", input.AccountID)
			s.logger.Error(l, "create-getting-account-wallet", msg)
			return domain.NewError(msg, domain.ErrorCodeNotFound)
		}
		transaction.Wallet = *wallet

		l.Interval = time.Since(t)
		s.logger.Infof(l, "create-getting-account-wallet", "got wallet for account %s", input.AccountID)

		return nil
	})

	g.Go(func() error {
		l := logOptions
		t := time.Now()
		operationType, err := s.operationTypeRepo.GetByExternalID(input.OperationTypeID)
		if err != nil {
			s.logger.Error(l, "create-getting-operation-type", err.Error())
			return errors.New("failed on trying to get operation type")
		}
		if operationType == nil {
			msg := fmt.Sprintf("the operation type %s was not found", input.OperationTypeID)
			s.logger.Error(l, "create-getting-operation-type", msg)
			return domain.NewError(msg, domain.ErrorCodeNotFound)
		}
		transaction.OperationType = *operationType

		l.Interval = time.Since(t)
		s.logger.Infof(l, "create-getting-operation-type", "got operation type %s", input.AccountID)

		return nil
	})

	if err := g.Wait(); err != nil {
		return empty, err
	}

	isNegativeOperation := transaction.OperationType.Negative
	if (isNegativeOperation && input.Amount > 0) || (!isNegativeOperation && input.Amount < 0) {
		operationKind := "negative"
		if !isNegativeOperation {
			operationKind = "non " + operationKind
		}
		msg := fmt.Sprintf("cant create %s transaction with amount %.2f", operationKind, input.Amount)
		s.logger.Error(logOptions, "create-validating-transaction-amount", msg)
		return empty, domain.NewError(msg, domain.ErrorCodeInvalidAmount)
	}

	var err error
	t := time.Now()
	transaction, err = s.transactionRepo.Insert(transaction)
	if err != nil {
		s.logger.Error(logOptions, "create-inserting-transaction", err.Error())
		return empty, errors.New("failed on trying to create transaction")
	}
	logOptions.Interval = time.Since(t)
	s.logger.Infof(logOptions, "create-inserting-transction", "created transaction %s", transaction.ExternalID)

	return domain.TransactionOutput{
		ID:              transaction.ExternalID,
		Amount:          transaction.Amount,
		EventDate:       transaction.EventDate,
		AccountID:       transaction.Wallet.AccountID,
		OperationTypeID: transaction.OperationType.ExternalID,
	}, nil
}
