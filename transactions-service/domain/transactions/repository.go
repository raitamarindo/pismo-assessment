package transactions

import (
	"fmt"

	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
)

// Repository ...
type Repository struct {
	database infra.DatabaseClient
}

// NewRepository ...
func NewRepository(database infra.DatabaseClient) Repository {
	return Repository{
		database: database,
	}
}

// Insert ...
func (r Repository) Insert(transaction domain.Transaction) (domain.Transaction, error) {
	empty := domain.Transaction{}
	tx, err := r.database.BeginTx()
	if err != nil {
		return empty, err
	}

	transaction, err = r.insert(transaction, tx)
	if err != nil {
		r.database.RollbackTx(tx)
		return empty, err
	}
	r.database.CommitTx(tx)

	return transaction, nil
}

// InsertTx ...
func (r Repository) InsertTx(transaction domain.Transaction, tx infra.Tx) (domain.Transaction, error) {
	return r.insert(transaction, tx)
}

func (r Repository) insert(transaction domain.Transaction, tx infra.Tx) (domain.Transaction, error) {
	empty := domain.Transaction{}
	_, err := r.database.ExecuteTx(
		tx,
		"INSERT INTO transactions(external_id, amount, event_date, wallet_id, operation_type_id) VALUES($1, $2, $3, $4, $5)",
		transaction.ExternalID,
		transaction.Amount,
		transaction.EventDate,
		transaction.Wallet.ID,
		transaction.OperationType.ID,
	)
	if err != nil {
		return empty, err
	}

	result, err := r.database.ExecuteQueryTx(tx, "SELECT id FROM transactions WHERE external_id = $1", transaction.ExternalID)
	if err != nil {
		return empty, err
	}
	defer result.Close()

	var id uint
	if result.Next() {
		err = result.Scan(&id)
	}
	if err != nil {
		return empty, err
	}
	if result.Err() != nil {
		return empty, result.Err()
	}
	if id == 0 {
		return empty, fmt.Errorf("created transaction %s was not found on database", transaction.ExternalID)
	}
	transaction.ID = id

	return transaction, nil
}
