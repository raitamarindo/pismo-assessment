package domain

import (
	"context"
	"time"

	"gitlab.com/raitamarindo/pismo-assessment/transactions-service/infra"
)

// TransactionService ...
type TransactionService interface {
	Create(ctx context.Context, input TransactionCreateInput) (TransactionOutput, error)
}

// TransactionRepository ...
type TransactionRepository interface {
	Insert(transaction Transaction) (Transaction, error)
	InsertTx(transaction Transaction, tx infra.Tx) (Transaction, error)
}

// WalletService ...
type WalletService interface {
	CreateMany(ctx context.Context, inputs []WalletCreateInput) ([]WalletOutput, error)
}

// WalletRepository ...
type WalletRepository interface {
	GetByAccountID(accountID string) (*Wallet, error)
	Insert(wallets ...Wallet) ([]Wallet, error)
}

// OperationTypeRepository ...
type OperationTypeRepository interface {
	GetByExternalID(externalID string) (*OperationType, error)
}

// TransactionCreateInput ...
type TransactionCreateInput struct {
	AccountID       string  `json:"account_id"`
	OperationTypeID string  `json:"operation_type_id"`
	Amount          float32 `json:"amount"`
}

// TransactionOutput ...
type TransactionOutput struct {
	ID              string    `json:"transaction_id"`
	AccountID       string    `json:"account_id"`
	OperationTypeID string    `json:"operation_type_id"`
	Amount          float32   `json:"amount"`
	EventDate       time.Time `json:"event_date"`
}

// Transaction ...
type Transaction struct {
	ID            uint
	ExternalID    string
	Amount        float32
	Wallet        Wallet
	OperationType OperationType
	EventDate     time.Time
}

// WalletCreateInput ...
type WalletCreateInput struct {
	AccountID string `json:"account_id"`
}

// WalletOutput ...
type WalletOutput struct {
	ID        string `json:"wallet_id"`
	AccountID string `json:"account_id"`
}

// Wallet ...
type Wallet struct {
	ID         uint
	ExternalID string
	AccountID  string
}

// OperationType ...
type OperationType struct {
	ID          uint
	ExternalID  string
	Description string
	Negative    bool
}

// Error ...
type Error interface {
	Error() string
	Code() ErrorCode
}

type domainError struct {
	Message string
	ErrCode ErrorCode
}

// Error ...
func (e domainError) Error() string {
	return e.Message
}

func (e domainError) Code() ErrorCode {
	return e.ErrCode
}

// NewError creates a new API error
func NewError(message string, errCode ErrorCode) Error {
	return domainError{
		Message: message,
		ErrCode: errCode,
	}
}

type (
	// ErrorCode ...
	ErrorCode string
	// ContextKey ...
	ContextKey string
)

const (
	// ErrorCodeNotFound ...
	ErrorCodeNotFound ErrorCode = "NotFound"
	// ErrorCodeMissingAccountID ...
	ErrorCodeMissingAccountID ErrorCode = "MissingAccountID"
	// ErrorCodeMissingOperationTypeID ...
	ErrorCodeMissingOperationTypeID ErrorCode = "MissingOperationTypeID"
	// ErrorCodeMissingAmount ...
	ErrorCodeMissingAmount ErrorCode = "MissingAmount"
	// ErrorCodeInvalidAmount ...
	ErrorCodeInvalidAmount ErrorCode = "InvalidAmount"
)

const (
	// ContextKeyTraceID ...
	ContextKeyTraceID ContextKey = "trace-id"
)
