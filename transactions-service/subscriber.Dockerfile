FROM golang:1.15-alpine3.12 as builder

ENV CGO_ENABLED=0

WORKDIR /app

COPY . .
RUN go mod download
RUN go build -o /subscriber -mod=readonly ./cmd/subscriber/main.go

FROM alpine

COPY --from=builder /subscriber /subscriber

ENTRYPOINT [ "/subscriber" ]