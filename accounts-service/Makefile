#!make
path=./...

op=up
steps=0

-include config.env
export

GOPATH=$(shell go env GOPATH)

setup:
	GO111MODULE=off go get -u github.com/kyoh86/richgo
	GO111MODULE=off go get -u golang.org/x/lint/golint
	GO111MODULE=off go get -u github.com/golang/mock/mockgen

api-run:
	go run cmd/api/main.go

fmt:
	go fmt $(path)
	find . -name \*.go -exec goimports -w {} \;

mock:
	rm -f mocks/domainsmocks.go
	rm -f mocks/inframocks.go
	$(GOPATH)/bin/mockgen -source=domain/contracts.go -destination=mocks/domainmocks.go -package=mocks
	$(GOPATH)/bin/mockgen -source=infra/contracts.go -destination=mocks/inframocks.go -package=mocks

test: mock
	$(GOPATH)/bin/richgo test $(path) -cover

coverage:
	go test -coverprofile=cover.out $(path)
	go tool cover -func=cover.out || true
	rm cover.out

lint:
	$(GOPATH)/bin/golint -set_exit_status -min_confidence 0.9 $(path)
	@echo "Golint found no problems on your code!"

api-image:
	docker build -t accounts-api:latest -f ./api.Dockerfile .
