package rabbitmq

import (
	"encoding/json"

	"github.com/streadway/amqp"
)

// Client ...
type Client struct {
	connectionURI string
}

// NewClient ...
func NewClient(connectionURI string) Client {
	return Client{
		connectionURI: connectionURI,
	}
}

// Publish ...
func (c Client) Publish(topic string, payload interface{}) error {
	conn, err := amqp.Dial(c.connectionURI)
	if err != nil {
		return err
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		return err
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		topic,    // name
		"fanout", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	if err != nil {
		return err
	}

	rawBody, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	return ch.Publish(topic, "", false, false, amqp.Publishing{
		ContentType: "application/json",
		Body:        rawBody,
	})
}
