package postgres

import (
	"errors"
	"fmt"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/infra"
)

func TestExecuteQuery(t *testing.T) {
	var sqlMock sqlmock.Sqlmock
	withMock := func(runner func(t *testing.T, client Client)) func(t *testing.T) {
		return func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			sqlMock = mock

			runner(t, Client{db: db})
		}
	}

	t.Run("should run the correct query in DB", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectQuery("SELECT ID, NAME FROM USERS")

		client.ExecuteQuery("SELECT ID, NAME FROM USERS")
		if err := sqlMock.ExpectationsWereMet(); err != nil {
			t.Error(err)
		}
	}))

	t.Run("should return one row from the database when database returns one row", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectQuery("SELECT ID, NAME FROM USERS").WillReturnRows(createRows(1))

		results, _ := client.ExecuteQuery("SELECT ID, NAME FROM USERS")

		count := 0
		for results.Next() {
			count++
		}

		assert.Equal(t, count, 1)
	}))

	t.Run("should return multiple rows from the database when database returns multiple rows", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectQuery("SELECT ID, NAME FROM USERS").WillReturnRows(createRows(2))

		results, _ := client.ExecuteQuery("SELECT ID, NAME FROM USERS")

		count := 0
		for results.Next() {
			count++
		}

		assert.Equal(t, count, 2)
	}))

	t.Run("should return no rows from the database when database returns no rows", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectQuery("SELECT ID, NAME FROM USERS").WillReturnRows(createRows(0))

		results, _ := client.ExecuteQuery("SELECT ID, NAME FROM USERS")
		if err := sqlMock.ExpectationsWereMet(); err != nil {
			t.Error(err)
		}

		count := 0
		for results.Next() {
			count++
		}

		assert.Equal(t, count, 0)
	}))
}

func TestExecute(t *testing.T) {
	var sqlMock sqlmock.Sqlmock
	withMock := func(runner func(t *testing.T, client Client)) func(t *testing.T) {
		return func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			sqlMock = mock

			runner(t, Client{db: db})
		}
	}

	t.Run("should run the correct sql in DB", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectExec("DELETE FROM USERS WHERE ID = 1")

		client.Execute("DELETE FROM USERS WHERE ID = 1")
		if err := sqlMock.ExpectationsWereMet(); err != nil {
			t.Error(err)
		}
	}))

	t.Run("should return last insert id from the database when database returns last insert id", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectExec(regexp.QuoteMeta("INSERT INTO USER (ID, NAME) VALUES (1, 'user-name')")).
			WillReturnResult(sqlmock.NewResult(1, 1))

		result, err := client.Execute("INSERT INTO USER (ID, NAME) VALUES (1, 'user-name')")
		if err != nil {
			t.Fatal("unexpected error on executing sql: " + err.Error())
		}
		lastID, err := result.LastInsertId()
		if err != nil {
			t.Fatal("unexpected error on getting last insert id: " + err.Error())
		}

		assert.Equal(t, lastID, int64(1))
	}))

	t.Run("should return rows affected number from the database when database returns rows affected number", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectExec(regexp.QuoteMeta("UPDATE USERS SET NAME = 'updated-user-name' WHERE NAME = 'user-name'")).
			WillReturnResult(sqlmock.NewResult(0, 1000))

		result, err := client.Execute("UPDATE USERS SET NAME = 'updated-user-name' WHERE NAME = 'user-name'")
		if err != nil {
			t.Fatal("unexpected error executing sql: " + err.Error())
		}
		rowsAffected, err := result.RowsAffected()
		if err != nil {
			t.Fatal("unexpected error on getting rows affected: " + err.Error())
		}

		assert.Equal(t, rowsAffected, int64(1000))
	}))

	t.Run("should return error from the database when database returns an error", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectExec("DELETE FROM USERS").WillReturnError(errors.New("any-error"))

		_, err := client.Execute("DELETE FROM USERS")
		if err == nil {
			t.Fatal("expected error was not received")
		}

		assert.Contains(t, err.Error(), "any-error")
	}))
}

func TestBeginTx(t *testing.T) {
	var sqlMock sqlmock.Sqlmock
	withMock := func(runner func(t *testing.T, client Client)) func(t *testing.T) {
		return func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			sqlMock = mock

			runner(t, Client{db: db})
		}
	}

	t.Run("should return a transaction when database begins one with success", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectBegin()

		tx, err := client.BeginTx()

		assert.Nil(t, err)
		assert.NotNil(t, tx.Tx)
	}))

	t.Run("should return error when database fail on begin transaction", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectBegin().WillReturnError(errors.New("any-error"))

		tx, err := client.BeginTx()
		if err == nil {
			t.Fatal("expected error was not received")
		}

		assert.Contains(t, err.Error(), "any-error")
		assert.Nil(t, tx.Tx)
	}))
}

func TestCommitTx(t *testing.T) {
	var sqlMock sqlmock.Sqlmock
	var txMock infra.Tx
	withMock := func(runner func(t *testing.T, client Client)) func(t *testing.T) {
		return func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			mock.ExpectBegin()
			tx, err := db.Begin()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			txMock = infra.Tx{
				Tx: tx,
			}
			sqlMock = mock

			runner(t, Client{db: db})
		}
	}

	t.Run("should return error when receive nil transaction", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectCommit().WillReturnError(errors.New("any-error"))

		err := client.CommitTx(infra.Tx{})

		assert.EqualError(t, err, "tx must be opened before commit")
	}))

	t.Run("should commit a transaction when database commits with success", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectCommit()

		err := client.CommitTx(txMock)

		assert.Nil(t, err)
	}))

	t.Run("should return error when database fail on commit transaction", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectCommit().WillReturnError(errors.New("any-error"))

		err := client.CommitTx(txMock)
		if err == nil {
			t.Fatal("expected error was not received")
		}

		assert.Contains(t, err.Error(), "any-error")
	}))
}

func TestRollbackTx(t *testing.T) {
	var sqlMock sqlmock.Sqlmock
	var txMock infra.Tx
	withMock := func(runner func(t *testing.T, client Client)) func(t *testing.T) {
		return func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			mock.ExpectBegin()
			tx, err := db.Begin()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			txMock = infra.Tx{
				Tx: tx,
			}
			sqlMock = mock

			runner(t, Client{db: db})
		}
	}

	t.Run("should do nothing when receive nil transaction", withMock(func(t *testing.T, client Client) {
		client.RollbackTx(infra.Tx{})
	}))

	t.Run("should rollback a transaction when called with not nil transaction", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectRollback()

		client.RollbackTx(txMock)
	}))
}

func TestExecuteQueryTx(t *testing.T) {
	var sqlMock sqlmock.Sqlmock
	var txMock infra.Tx
	withMock := func(runner func(t *testing.T, client Client)) func(t *testing.T) {
		return func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			mock.ExpectBegin()
			tx, err := db.Begin()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			txMock = infra.Tx{
				Tx: tx,
			}
			sqlMock = mock

			runner(t, Client{db: db})
		}
	}

	t.Run("should return an error when receive nil transaction", withMock(func(t *testing.T, client Client) {
		_, err := client.ExecuteQueryTx(infra.Tx{}, "SELECT * FROM USERS")

		assert.EqualError(t, err, "tx must be opened before query")
	}))

	t.Run("should run the correct query in DB", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectQuery("SELECT ID, NAME FROM USERS")

		client.ExecuteQueryTx(txMock, "SELECT ID, NAME FROM USERS")
		if err := sqlMock.ExpectationsWereMet(); err != nil {
			t.Error(err)
		}
	}))

	t.Run("should return one row from the database when database returns one row", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectQuery("SELECT ID, NAME FROM USERS").WillReturnRows(createRows(1))

		results, _ := client.ExecuteQueryTx(txMock, "SELECT ID, NAME FROM USERS")

		count := 0
		for results.Next() {
			count++
		}

		assert.Equal(t, count, 1)
	}))

	t.Run("should return multiple rows from the database when database returns multiple rows", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectQuery("SELECT ID, NAME FROM USERS").WillReturnRows(createRows(2))

		results, _ := client.ExecuteQueryTx(txMock, "SELECT ID, NAME FROM USERS")

		count := 0
		for results.Next() {
			count++
		}

		assert.Equal(t, count, 2)
	}))

	t.Run("should return no rows from the database when database returns no rows", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectQuery("SELECT ID, NAME FROM USERS").WillReturnRows(createRows(0))

		results, _ := client.ExecuteQueryTx(txMock, "SELECT ID, NAME FROM USERS")
		if err := sqlMock.ExpectationsWereMet(); err != nil {
			t.Error(err)
		}

		count := 0
		for results.Next() {
			count++
		}

		assert.Equal(t, count, 0)
	}))
}

func TestExecuteTx(t *testing.T) {
	var sqlMock sqlmock.Sqlmock
	var txMock infra.Tx
	withMock := func(runner func(t *testing.T, client Client)) func(t *testing.T) {
		return func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			mock.ExpectBegin()
			tx, err := db.Begin()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			txMock = infra.Tx{
				Tx: tx,
			}
			sqlMock = mock

			runner(t, Client{db: db})
		}
	}

	t.Run("should return an error when receive nil transaction", withMock(func(t *testing.T, client Client) {
		_, err := client.ExecuteTx(infra.Tx{}, "DELETE FROM USERS")

		assert.EqualError(t, err, "tx must be opened before exec")
	}))

	t.Run("should run the correct sql in DB", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectExec("DELETE FROM USERS WHERE ID = 1")

		client.ExecuteTx(txMock, "DELETE FROM USERS WHERE ID = 1")
		if err := sqlMock.ExpectationsWereMet(); err != nil {
			t.Error(err)
		}
	}))

	t.Run("should return last insert id from the database when database returns last insert id", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectExec(regexp.QuoteMeta("INSERT INTO USER (ID, NAME) VALUES (1, 'user-name')")).
			WillReturnResult(sqlmock.NewResult(1, 1))

		result, err := client.ExecuteTx(txMock, "INSERT INTO USER (ID, NAME) VALUES (1, 'user-name')")
		if err != nil {
			t.Fatal("unexpected error on executing sql: " + err.Error())
		}
		lastID, err := result.LastInsertId()
		if err != nil {
			t.Fatal("unexpected error on getting last insert id: " + err.Error())
		}

		assert.Equal(t, lastID, int64(1))
	}))

	t.Run("should return rows affected number from the database when database returns rows affected number", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectExec("UPDATE USERS SET NAME = 'updated-user-name' WHERE NAME = 'user-name'").
			WillReturnResult(sqlmock.NewResult(0, 1000))

		result, err := client.ExecuteTx(txMock, "UPDATE USERS SET NAME = 'updated-user-name' WHERE NAME = 'user-name'")
		if err != nil {
			t.Fatal("unexpected error executing sql: " + err.Error())
		}
		rowsAffected, err := result.RowsAffected()
		if err != nil {
			t.Fatal("unexpected error on getting rows affected: " + err.Error())
		}

		assert.Equal(t, rowsAffected, int64(1000))
	}))

	t.Run("should return error from the database when database returns an error", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectExec("DELETE FROM USERS").WillReturnError(errors.New("any-error"))

		_, err := client.ExecuteTx(txMock, "DELETE FROM USERS")
		if err == nil {
			t.Fatal("expected error was not received")
		}

		assert.Contains(t, err.Error(), "any-error")
	}))
}

func TestWarmup(t *testing.T) {
	var sqlMock sqlmock.Sqlmock
	withMock := func(runner func(t *testing.T, client Client)) func(t *testing.T) {
		return func(t *testing.T) {
			db, mock, err := sqlmock.New()
			if err != nil {
				t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
			}
			defer db.Close()

			sqlMock = mock

			runner(t, Client{db: db})
		}
	}

	t.Run("should not return an error when not receiving error from database", withMock(func(t *testing.T, client Client) {
		row := sqlmock.NewRows([]string{"id"})
		row.AddRow(1)
		sqlMock.ExpectQuery("SELECT 1").WillReturnRows(row)

		err := client.WarmUp()

		assert.Nil(t, err)
	}))

	t.Run("should return an error when receiving error from database", withMock(func(t *testing.T, client Client) {
		sqlMock.ExpectQuery("SELECT 1").WillReturnError(errors.New("custom-error"))

		err := client.WarmUp()

		assert.EqualError(t, err, "error while trying to warm up the api: custom-error")
	}))

	t.Run("should return an error when receiving error while scrolling the result", withMock(func(t *testing.T, client Client) {
		row := sqlmock.NewRows([]string{"id"})
		row.AddRow("a")
		sqlMock.ExpectQuery("SELECT 1").WillReturnRows(row)

		err := client.WarmUp()

		assert.Contains(t, err.Error(), "error while scanning:")
	}))
}

func createRows(total int) *sqlmock.Rows {
	rows := sqlmock.NewRows([]string{"id", "name"})

	for i := 1; i <= total; i++ {
		rows.AddRow(i, fmt.Sprintf("User %v", i))
	}

	return rows
}
