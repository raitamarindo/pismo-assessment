CREATE TABLE IF NOT EXISTS accounts (
    id SERIAL PRIMARY KEY,
    external_id CHAR(36) NOT NULL UNIQUE,
    document_number VARCHAR(100) NOT NULL
);
