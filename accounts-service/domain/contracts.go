package domain

import (
	"context"

	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/infra"
)

// AccountService ...
type AccountService interface {
	GetByID(ctx context.Context, id string) (AccountOutput, error)
	Create(ctx context.Context, input AccountCreateInput) (AccountOutput, error)
}

// AccountRepository ...
type AccountRepository interface {
	GetByID(id string) (*Account, error)
	Insert(account Account) (Account, error)
	InsertTx(account Account, tx infra.Tx) (Account, error)
}

// AccountCreateInput ...
type AccountCreateInput struct {
	DocumentNumber string `json:"document_number"`
}

// AccountOutput ...
type AccountOutput struct {
	ID             string `json:"account_id"`
	DocumentNumber string `json:"document_number"`
}

// AccountCreatedEvent ...
type AccountCreatedEvent struct {
	TraceID string          `json:"trace_id"`
	Data    []AccountOutput `json:"data"`
}

// Account ...
type Account struct {
	ID             uint
	ExternalID     string
	DocumentNumber string
}

// Error ...
type Error interface {
	Error() string
	Code() ErrorCode
}

type domainError struct {
	Message string
	ErrCode ErrorCode
}

// Error ...
func (e domainError) Error() string {
	return e.Message
}

func (e domainError) Code() ErrorCode {
	return e.ErrCode
}

// NewError creates a new API error
func NewError(message string, errCode ErrorCode) Error {
	return domainError{
		Message: message,
		ErrCode: errCode,
	}
}

type (
	// ErrorCode ...
	ErrorCode string
	// ContextKey ...
	ContextKey string
)

const (
	// ErrorCodeNotFound ...
	ErrorCodeNotFound ErrorCode = "NotFound"
	// ErrorCodeMissingDocumentNumber ...
	ErrorCodeMissingDocumentNumber ErrorCode = "MissingDocumentNumber"
	// ErrorCodeMissingID ...
	ErrorCodeMissingID ErrorCode = "ErrorCodeMissingID"
)

const (
	// ContextKeyTraceID ...
	ContextKeyTraceID ContextKey = "trace-id"
)
