package accounts

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/infra"
	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/mocks"
)

func TestService_GetByID(t *testing.T) {
	var accountRepo *mocks.MockAccountRepository

	withMock := func(runner func(t *testing.T, s Service)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			accountRepo = mocks.NewMockAccountRepository(ctrl)
			messageBroker := mocks.NewMockMessageBroker(ctrl)
			logger := mocks.NewMockLogProvider(ctrl)

			logger.EXPECT().Error(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()
			logger.EXPECT().Infof(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()

			runner(t, NewService("", accountRepo, messageBroker, logger))
		}
	}

	t.Run(
		"should return an error when id is empty",
		withMock(func(t *testing.T, s Service) {
			_, err := s.GetByID(context.TODO(), "")

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeMissingID, e.Code())
		}),
	)

	t.Run(
		"should return error when repository fail on finding account",
		withMock(func(t *testing.T, s Service) {
			id := "uuid-1"

			accountRepo.EXPECT().
				GetByID(id).
				Return(nil, errors.New("any error"))

			_, err := s.GetByID(context.TODO(), id)

			assert.EqualError(t, err, "failed on trying to get account from database")
		}),
	)

	t.Run(
		"should return a not found error when repository returns nil account without error",
		withMock(func(t *testing.T, s Service) {
			id := "uuid-1"

			accountRepo.EXPECT().
				GetByID(id).
				Return(nil, nil)

			_, err := s.GetByID(context.TODO(), id)

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeNotFound, e.Code())
		}),
	)

	t.Run(
		"should return an account when it is found by repository",
		withMock(func(t *testing.T, s Service) {
			id := "uuid-1"
			expected := domain.AccountOutput{
				ID:             id,
				DocumentNumber: "123456",
			}

			accountRepo.EXPECT().
				GetByID(id).
				Return(&domain.Account{
					ID:             1,
					ExternalID:     expected.ID,
					DocumentNumber: expected.DocumentNumber,
				}, nil)

			output, err := s.GetByID(context.TODO(), id)

			assert.Nil(t, err)
			assert.Equal(t, expected, output)
		}),
	)
}

func TestService_Create(t *testing.T) {
	var accountRepo *mocks.MockAccountRepository
	var messageBroker *mocks.MockMessageBroker
	var logger *mocks.MockLogProvider

	withMock := func(runner func(t *testing.T, s Service)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			accountRepo = mocks.NewMockAccountRepository(ctrl)
			messageBroker = mocks.NewMockMessageBroker(ctrl)
			logger = mocks.NewMockLogProvider(ctrl)

			logger.EXPECT().Info(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()
			logger.EXPECT().Infof(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()

			runner(t, NewService("accounts-created", accountRepo, messageBroker, logger))
		}
	}

	t.Run(
		"should return error when document number is empty",
		withMock(func(t *testing.T, s Service) {
			logger.EXPECT().Error(gomock.Any(), gomock.Any(), gomock.Any())

			_, err := s.Create(context.TODO(), domain.AccountCreateInput{})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			var e domain.Error
			assert.True(t, errors.As(err, &e))
			assert.Equal(t, domain.ErrorCodeMissingDocumentNumber, e.Code())
		}),
	)

	t.Run(
		"should return error when repository fail on creating account",
		withMock(func(t *testing.T, s Service) {
			input := domain.AccountCreateInput{DocumentNumber: "123456"}

			accountRepo.EXPECT().
				Insert(gomock.Any()).
				Return(domain.Account{}, errors.New("any error"))
			logger.EXPECT().Error(gomock.Any(), gomock.Any(), gomock.Any())

			_, err := s.Create(context.TODO(), input)

			assert.EqualError(t, err, "failed on trying to insert account on database")
		}),
	)

	t.Run(
		"should return the created account when repository inserts with success",
		withMock(func(t *testing.T, s Service) {
			input := domain.AccountCreateInput{DocumentNumber: "123456"}
			expectedAccount := domain.Account{
				ID:             1,
				ExternalID:     "uuid-1",
				DocumentNumber: "123456",
			}

			accountRepo.EXPECT().
				Insert(gomock.Any()).
				Return(expectedAccount, nil)
			messageBroker.EXPECT().Publish(gomock.Any(), gomock.Any()).Return(nil)

			output, err := s.Create(context.TODO(), input)

			assert.Nil(t, err)
			assert.Equal(t, expectedAccount.DocumentNumber, output.DocumentNumber)
			assert.Equal(t, expectedAccount.ExternalID, output.ID)
		}),
	)

	t.Run(
		"should emit a created account event when repository inserts with success",
		withMock(func(t *testing.T, s Service) {
			input := domain.AccountCreateInput{DocumentNumber: "123456"}
			expectedAccount := domain.AccountOutput{
				ID:             "uuid-1",
				DocumentNumber: "123456",
			}
			var publishedAccounts []domain.AccountOutput

			accountRepo.EXPECT().
				Insert(gomock.Any()).
				Return(domain.Account{
					ID:             1,
					ExternalID:     expectedAccount.ID,
					DocumentNumber: expectedAccount.DocumentNumber,
				}, nil)
			messageBroker.EXPECT().
				Publish(gomock.Any(), gomock.Any()).
				DoAndReturn(func(topic string, payload interface{}) error {
					event, _ := payload.(domain.AccountCreatedEvent)
					publishedAccounts = event.Data
					return nil
				})

			_, err := s.Create(context.TODO(), input)

			assert.Nil(t, err)
			assert.NotEmpty(t, publishedAccounts)
			assert.Contains(t, publishedAccounts, expectedAccount)
		}),
	)

	t.Run(
		"should just log error when message broker fails to emit event",
		withMock(func(t *testing.T, s Service) {
			input := domain.AccountCreateInput{DocumentNumber: "123456"}
			expectedAccount := domain.AccountOutput{
				ID:             "uuid-1",
				DocumentNumber: "123456",
			}
			var loggedMsg string

			accountRepo.EXPECT().
				Insert(gomock.Any()).
				Return(domain.Account{
					ID:             1,
					ExternalID:     expectedAccount.ID,
					DocumentNumber: expectedAccount.DocumentNumber,
				}, nil)
			messageBroker.EXPECT().
				Publish(gomock.Any(), gomock.Any()).
				Return(errors.New("any error"))
			logger.EXPECT().
				Error(gomock.Any(), gomock.Any(), gomock.Any()).
				Do(func(options infra.LogOptions, context, message string) {
					loggedMsg = message
				})

			_, err := s.Create(context.TODO(), input)

			assert.Nil(t, err)
			assert.Equal(t, "any error", loggedMsg)
		}),
	)
}
