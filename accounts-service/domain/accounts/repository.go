package accounts

import (
	"fmt"

	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/infra"
)

// Repository ...
type Repository struct {
	database infra.DatabaseClient
}

// NewRepository ...
func NewRepository(database infra.DatabaseClient) Repository {
	return Repository{
		database: database,
	}
}

// GetByID ...
func (r Repository) GetByID(externalID string) (*domain.Account, error) {
	query := `
	SELECT
		id,
		external_id,
		document_number
	FROM
		accounts
	WHERE
		external_id = $1`

	result, err := r.database.ExecuteQuery(query, externalID)
	if err != nil {
		return nil, err
	}
	defer result.Close()

	var account domain.Account
	if result.Next() {
		err = result.Scan(&account.ID, &account.ExternalID, &account.DocumentNumber)
	}
	if err != nil {
		return nil, err
	}
	if result.Err() != nil {
		return nil, result.Err()
	}
	if account.ID == 0 {
		return nil, nil
	}

	return &account, nil
}

// Insert ...
func (r Repository) Insert(account domain.Account) (domain.Account, error) {
	empty := domain.Account{}
	tx, err := r.database.BeginTx()
	if err != nil {
		return empty, err
	}

	account, err = r.insert(account, tx)
	if err != nil {
		r.database.RollbackTx(tx)
		return empty, err
	}
	r.database.CommitTx(tx)

	return account, nil
}

// InsertTx ...
func (r Repository) InsertTx(account domain.Account, tx infra.Tx) (domain.Account, error) {
	return r.insert(account, tx)
}

func (r Repository) insert(account domain.Account, tx infra.Tx) (domain.Account, error) {
	empty := domain.Account{}
	_, err := r.database.ExecuteTx(
		tx,
		"INSERT INTO accounts(external_id, document_number) VALUES($1, $2)",
		account.ExternalID,
		account.DocumentNumber,
	)
	if err != nil {
		return empty, err
	}

	result, err := r.database.ExecuteQueryTx(tx, "SELECT id FROM accounts WHERE external_id = $1", account.ExternalID)
	if err != nil {
		return empty, err
	}
	defer result.Close()

	var id uint
	if result.Next() {
		err = result.Scan(&id)
	}
	if err != nil {
		return empty, err
	}
	if result.Err() != nil {
		return empty, result.Err()
	}
	if id == 0 {
		return empty, fmt.Errorf("created account %s was not found on database", account.ExternalID)
	}
	account.ID = id

	return account, nil
}
