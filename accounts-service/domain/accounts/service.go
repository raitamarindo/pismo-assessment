package accounts

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/infra"
)

// Service ...
type Service struct {
	accountCreatedTopic string
	accountRepo         domain.AccountRepository
	messageBroker       infra.MessageBroker
	logger              infra.LogProvider
}

// NewService ...
func NewService(
	accountCreatedTopic string,
	accountRepo domain.AccountRepository,
	messageBroker infra.MessageBroker,
	logger infra.LogProvider,
) Service {
	return Service{
		accountCreatedTopic: accountCreatedTopic,
		accountRepo:         accountRepo,
		messageBroker:       messageBroker,
		logger:              logger,
	}
}

// GetByID ...
func (s Service) GetByID(ctx context.Context, id string) (domain.AccountOutput, error) {
	traceID, _ := ctx.Value(domain.ContextKeyTraceID).(string)
	logOptions := infra.LogOptions{
		TraceID:   traceID,
		AccountID: id,
	}
	empty := domain.AccountOutput{}

	if id == "" {
		msg := "missing id to find account"
		s.logger.Error(logOptions, "get-by-id-validating-id", msg)
		return empty, domain.NewError(msg, domain.ErrorCodeMissingID)
	}

	t := time.Now()
	account, err := s.accountRepo.GetByID(id)
	if err != nil {
		s.logger.Error(logOptions, "get-by-id-finding-account", err.Error())
		return empty, errors.New("failed on trying to get account from database")
	}
	if account == nil {
		msg := fmt.Sprintf("account %s was not found on database", id)
		s.logger.Error(logOptions, "get-by-id-finding-account", msg)
		return empty, domain.NewError(msg, domain.ErrorCodeNotFound)
	}
	logOptions.Interval = time.Since(t)
	s.logger.Infof(logOptions, "get-by-id-finding-account", "account with id %s was found", id)

	return domain.AccountOutput{
		ID:             account.ExternalID,
		DocumentNumber: account.DocumentNumber,
	}, nil
}

// Create ...
func (s Service) Create(ctx context.Context, input domain.AccountCreateInput) (domain.AccountOutput, error) {
	traceID, _ := ctx.Value(domain.ContextKeyTraceID).(string)
	empty := domain.AccountOutput{}
	logOptions := infra.LogOptions{TraceID: traceID}
	if input.DocumentNumber == "" {
		s.logger.Error(logOptions, "create-validating-input", "got empty document number on create account")
		return empty, domain.NewError("account document number can not be empty", domain.ErrorCodeMissingDocumentNumber)
	}

	account := domain.Account{
		ExternalID:     uuid.New().String(),
		DocumentNumber: input.DocumentNumber,
	}
	logOptions.AccountID = account.ExternalID
	t := time.Now()
	account, err := s.accountRepo.Insert(account)
	if err != nil {
		s.logger.Error(logOptions, "create-inserting-account-database", err.Error())
		return empty, errors.New("failed on trying to insert account on database")
	}
	logOptions.Interval = time.Since(t)
	s.logger.Infof(logOptions, "create-inserting-account-database", "account %s inserted on database")

	output := domain.AccountOutput{
		ID:             account.ExternalID,
		DocumentNumber: account.DocumentNumber,
	}

	event := domain.AccountCreatedEvent{
		TraceID: traceID,
		Data:    []domain.AccountOutput{output},
	}
	t = time.Now()
	err = s.messageBroker.Publish(s.accountCreatedTopic, event)
	if err != nil {
		s.logger.Error(logOptions, "create-publishing-created-account-event", err.Error())
	} else {
		s.logger.Info(logOptions, "create-publishing-created-account-event", "published account created on message")
	}

	return output, nil
}
