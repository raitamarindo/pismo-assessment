package accounts

import (
	"database/sql"
	"errors"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/domain"
	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/infra"
	"gitlab.com/raitamarindo/pismo-assessment/accounts-service/mocks"
)

func TestRepository_GetByID(t *testing.T) {
	var databaseClient *mocks.MockDatabaseClient
	var sqlMock sqlmock.Sqlmock

	withMock := func(runner func(t *testing.T, r Repository)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			var db *sql.DB
			var err error

			db, sqlMock, err = sqlmock.New()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			databaseClient = mocks.NewMockDatabaseClient(ctrl)

			databaseClient.EXPECT().
				ExecuteQuery(gomock.Any(), gomock.Any()).
				DoAndReturn(func(query string, args ...interface{}) (*sql.Rows, error) {
					return db.Query(query, args...)
				}).
				AnyTimes()

			runner(t, NewRepository(databaseClient))
		}
	}

	query := `
	SELECT
		id,
		external_id,
		document_number
	FROM
		accounts
	WHERE
		external_id = $1`

	t.Run(
		"should return an error when database fails on fetching row",
		withMock(func(t *testing.T, r Repository) {
			sqlMock.ExpectQuery(regexp.QuoteMeta(query)).
				WithArgs("uuid-1").
				WillReturnError(errors.New("any error"))

			_, err := r.GetByID("uuid-1")

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should return nil when database returns empty rows",
		withMock(func(t *testing.T, r Repository) {
			rows := sqlMock.NewRows([]string{"id", "external_id", "document_number"})

			sqlMock.ExpectQuery(regexp.QuoteMeta(query)).
				WithArgs("uuid-1").
				WillReturnRows(rows)

			account, err := r.GetByID("uuid-1")

			assert.Nil(t, err)
			assert.Nil(t, account)
		}),
	)

	t.Run(
		"should map row to account when database returns one row",
		withMock(func(t *testing.T, r Repository) {
			rows := sqlMock.NewRows([]string{"id", "external_id", "document_number"})
			rows.AddRow(1, "uuid-1", "123456")

			sqlMock.ExpectQuery(regexp.QuoteMeta(query)).
				WithArgs("uuid-1").
				WillReturnRows(rows)

			account, err := r.GetByID("uuid-1")

			assert.Nil(t, err)
			assert.Equal(t, &domain.Account{
				ID:             1,
				ExternalID:     "uuid-1",
				DocumentNumber: "123456",
			}, account)
		}),
	)
}

func TestRepository_Insert(t *testing.T) {
	var databaseClient *mocks.MockDatabaseClient
	var sqlMock sqlmock.Sqlmock

	withMock := func(runner func(t *testing.T, r Repository)) func(t *testing.T) {
		return func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			var db *sql.DB
			var err error

			db, sqlMock, err = sqlmock.New()
			if err != nil {
				t.Fatalf("error on stub creation: %s", err)
			}

			databaseClient = mocks.NewMockDatabaseClient(ctrl)

			databaseClient.EXPECT().
				ExecuteTx(gomock.Any(), gomock.Any(), gomock.Any()).
				DoAndReturn(func(tx infra.Tx, query string, args ...interface{}) (sql.Result, error) {
					return db.Exec(query, args...)
				}).
				AnyTimes()
			databaseClient.EXPECT().
				ExecuteQueryTx(gomock.Any(), gomock.Any(), gomock.Any()).
				DoAndReturn(func(tx infra.Tx, query string, args ...interface{}) (*sql.Rows, error) {
					return db.Query(query, args...)
				}).
				AnyTimes()

			runner(t, NewRepository(databaseClient))
		}
	}

	insertQuery := "INSERT INTO accounts(external_id, document_number) VALUES($1, $2)"
	selectQuery := "SELECT id FROM accounts WHERE external_id = $1"

	t.Run(
		"should return an error when session manager fails to begin transaction",
		withMock(func(t *testing.T, r Repository) {
			databaseClient.EXPECT().BeginTx().Return(infra.Tx{}, errors.New("any error"))

			_, err := r.Insert(domain.Account{})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should return an error when database fails on inserting row",
		withMock(func(t *testing.T, r Repository) {
			tx := infra.Tx{
				Tx: &sql.Tx{},
			}

			databaseClient.EXPECT().BeginTx().Return(tx, nil)
			sqlMock.ExpectExec(regexp.QuoteMeta(insertQuery)).
				WithArgs("uuid-1", "123456").
				WillReturnError(errors.New("any error"))
			databaseClient.EXPECT().RollbackTx(tx)

			_, err := r.Insert(domain.Account{
				ExternalID:     "uuid-1",
				DocumentNumber: "123456",
			})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should return an error when database fails on fetching row id",
		withMock(func(t *testing.T, r Repository) {
			tx := infra.Tx{}

			databaseClient.EXPECT().BeginTx().Return(tx, nil)
			sqlMock.ExpectExec(regexp.QuoteMeta(insertQuery)).
				WithArgs("uuid-1", "123456").
				WillReturnResult(sqlmock.NewResult(1, 1))
			sqlMock.ExpectQuery(regexp.QuoteMeta(selectQuery)).
				WithArgs("uuid-1").
				WillReturnError(errors.New("any error"))
			databaseClient.EXPECT().RollbackTx(tx)

			_, err := r.Insert(domain.Account{
				ExternalID:     "uuid-1",
				DocumentNumber: "123456",
			})

			if err == nil {
				t.Fatal("expected error was not found")
			}
			assert.EqualError(t, err, "any error")
		}),
	)

	t.Run(
		"should commit and map returned row to an account when database fetches the created account id",
		withMock(func(t *testing.T, r Repository) {
			tx := infra.Tx{
				Tx: &sql.Tx{},
			}
			rows := sqlmock.NewRows([]string{"id"})
			rows.AddRow(1)

			databaseClient.EXPECT().BeginTx().Return(tx, nil)
			sqlMock.ExpectExec(regexp.QuoteMeta(insertQuery)).
				WithArgs("uuid-1", "123456").
				WillReturnResult(sqlmock.NewResult(1, 1))
			sqlMock.ExpectQuery(regexp.QuoteMeta(selectQuery)).
				WithArgs("uuid-1").
				WillReturnRows(rows)
			databaseClient.EXPECT().CommitTx(tx).Return(nil)

			account, err := r.Insert(domain.Account{
				ExternalID:     "uuid-1",
				DocumentNumber: "123456",
			})

			assert.Nil(t, err)
			assert.Equal(t, uint(1), account.ID)
			assert.Equal(t, "uuid-1", account.ExternalID)
			assert.Equal(t, "123456", account.DocumentNumber)
		}),
	)
}
